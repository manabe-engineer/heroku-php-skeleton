#!/usr/bin/env bash
set -ex

if [ ${BASIC_AUTH_ON} = true ] ; then

	sed -ri -e "s!auth_basic off!auth_basic \"Auth basic\"!g" /app/nginx_app.conf
	echo -e "${BASIC_AUTH_USERNAME}:$(perl -le 'print crypt($ENV{"BASIC_AUTH_PASSWORD"}, rand(0xffffffff));')" > /app/.htpasswd

fi
