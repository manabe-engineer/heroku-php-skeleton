# PHP environment skelton for Heroku in docker

This is a framework for publishing a PHP environment on heroku.

In the local environment, you can develop and check your work on the Docker environment.

## Demo
https://php-skeleton-manabe.herokuapp.com/

* This application is published by Heroku's free plan. As such, it will go into sleep mode if there is no access for 30 minutes. **You will need to wait for a certain amount of time** because it will need to be restarted when you access it after that.
## Whole image

<div align="center">

![image](readme/heroku-skeleton.png "PHP environment skelton for Heroku in docker")

</div>



## Requirement

### [Docker](https://www.docker.com/)

- [How to install](https://docs.docker.com/get-docker/)

### [Git](https://git-scm.com/)

- [How to install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


### [Nginx server for local develop environment in docker](https://gitlab.com/manabe-engineer/nginx-for-local-env-in-docker)

- [How to install](https://gitlab.com/manabe-engineer/nginx-for-local-env-in-docker#installation)

## Installation

```console
$ git clone https://gitlab.com/manabe-engineer/heroku-php-skeleton.git
$ cd heroku-php-skeleton
$ make init
```

## Usage examples

### Local Environment Setting

1. Add new URL to hosts file

```h
127.0.0.1 heroku-php-skeleton.local-com
```
[How to Edit Your Hosts File on Linux, Windows, and macOS](https://linuxize.com/post/how-to-edit-your-hosts-file/)

2. Up docker compose
```console
$ cd heroku-php-skeleton
$ make up
```
3. Access "https://heroku-php-skeleton.local-com/" by web browser
<div align="center">

![image](readme/default-page.png "PHP environment skelton for Heroku in docker")

</div>



#### Note.1: How to change local URL
1. Change .env file

```
- APP_URL = heroku-php-skeleton.local-com
+ APP_URL = change-url.local-com
```

2. Add new URL to hosts file
```h
127.0.0.1 change-url.local-com
```

#### Note.2: How to modify php file
1. Modify app-html/public/index.php

### Deploy to Heroku using GitLab CI/CD

1. Sign up for Heroku, create an account, and check the API key for that account ("Account Setting" -> "API Key" ).

	Sign up: https://signup.heroku.com/


<div align="center">

![image](readme/account-setting.png "Account Setting")

</div>

<div align="center">

![image](readme/API-key.png "API key")

</div>

2. Create two applications, staging and production, on Heroku.


3. Create new pipeline, and register the above two applications in this pipeline.

	Reference: https://devcenter.heroku.com/articles/pipelines

<div align="center">

![image](readme/pipeline.jpg "pipeline in heroku")

</div>

4. Create New project on GitLab, and set CI/CD Variables.

	HEROKU_API_KEY: API key that was checked in "1."

	HEROKU_PRD_APPNAME: Production application name that was created in "2."

	HEROKU_STG_APPNAME: Staging application name that was created in "2."

<div align="center">

![image](readme/variables.png "Variables in GitLab CI/CD")

</div>

5. Change .env file

```
- HEROKU_STG_APP_NAME = php-skeleton-manabe-staging
- HEROKU_PRD_APP_NAME = php-skeleton-manabe
+ HEROKU_STG_APP_NAME = new-staging-application-name
+ HEROKU_PRD_APP_NAME = new-production-application-name
```

6. Commit gitLab project that was created in "4."

7. Push master branch in order to deploy staging application that was crated in "2."

8. Add protected tag in order to deploy production application that was crated in "2."


#### Note.1: How to set basic Authentication in Heroku

1. Set config vars in heroku application setting.

	BASIC_AUTH_ON: Set to "true" if you want to enable basic authentication, or set to any other value if you want to disable it.

	BASIC_AUTH_PASSWORD: Password for Basic Authentication

	BASIC_AUTH_USERNAME: User name for basic authentication

<div align="center">

![image](readme/config-vars.png "Config vars in heroku")

</div>

## Author
### MANABE yusuke
* E-mail: manabe.engineer@gmail.com

## Reference
- https://hub.appirio.jp/tech-blog/heroku-tips-4
- https://qiita.com/teracy55/items/2f9ecaafc0a042c1f6df
- https://koyacode.com/ci-rails-apps-from-gitlab-to-heroku/
- https://blog.takuchalle.dev/post/2018/08/08/cicd_railstutorial_to_heroku/
- https://about.gitlab.com/blog/2021/03/22/we-are-building-a-better-heroku/
- https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4
- https://blog.n-z.jp/blog/2017-07-13-gitlab-ci-heroku.html
- https://mistymagich.wordpress.com/2016/02/12/nginx-basic-authentication-in-php-on-heroku/
- https://qiita.com/keb/items/448cde81e49fa17f35ab
- https://qiita.com/takehanKosuke/items/d27e2041148381d4ecf4
- https://github.com/alex041291/heroku-nginx-basic-auth
- https://obel.hatenablog.jp/entry/20200216/1581840180

## License

"PHP environment skelton for Heroku in docker" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
